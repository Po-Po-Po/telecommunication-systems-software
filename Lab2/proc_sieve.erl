-module(proc_sieve).

-export([generate/1]).
-export([generate_sieve/1]).
-export([sieve/0]).

generate_sieve(MaxN) ->
	lists:foreach(fun(X)->io:format("~w ",[X]) end, generate(MaxN)).

generate(MaxN) when MaxN > 1 ->
	PID = spawn(proc_sieve, sieve, []),
	generate_helper(2, MaxN, PID);

generate(_) ->
	[].

generate_helper(CurNum, MaxN, PID) when CurNum =< MaxN ->
	PID ! CurNum, 
	generate_helper(CurNum + 1, MaxN, PID);

generate_helper(_, _, PID) ->
	PID ! {done, self()},
	receive
		{result, List} ->
			List;
		_ ->
			{error, "Wrong output"}
	end.

sieve() ->
	receive
		FilterBy ->
			sieve_helper(FilterBy, nil, nil)
	end.

sieve_helper(Filter, RedirectPid, SavedBackPID) ->
	receive
		{done, BackPID} ->
			if
				RedirectPid == nil ->
					BackPID ! {result, [Filter]};
				true ->
					RedirectPid ! {done, self()},
					sieve_helper(Filter, nil, BackPID)
			end;
		{result, List} ->
			if
				SavedBackPID =/= nil ->
						SavedBackPID ! {result, [Filter | List]};
				true ->
					nil
			end;
		CurNum when (CurNum rem Filter) =/= 0 ->
			if
				RedirectPid == nil ->
					PID = spawn(proc_sieve, sieve, []),
					PID ! CurNum,
					sieve_helper(Filter, PID, SavedBackPID);
				true ->
					RedirectPid ! CurNum,
					sieve_helper(Filter, RedirectPid, SavedBackPID)
			end;
		_ ->
			sieve_helper(Filter, RedirectPid, SavedBackPID)
	end.
