-module(mobius).

-export([is_prime/1]).
-export([prime_factors/1]).
-export([is_square_multiple/1]).
-export([find_square_multiples/2]).

is_prime(N) ->
	is_prime_next(2, N).

is_prime_next(Divisor, N) when (Divisor * Divisor) =< N ->
	if
		(N rem Divisor) == 0 ->
			false;
		true ->
			is_prime_next(Divisor + 1, N)
	end;

is_prime_next(_, _) -> true.

prime_factors(0) ->
	[];

prime_factors(N) ->
	prime_factors_next(N, 2, [1]).

prime_factors_next(N, CoMultiplier, CurResult) when CoMultiplier =< N ->
	if
		(N rem CoMultiplier) == 0 ->
			IsPrime = is_prime(CoMultiplier),
			if 
				IsPrime == true ->
					prime_factors_next(N, CoMultiplier + 1, [CoMultiplier|CurResult]);
				true ->
					prime_factors_next(N, CoMultiplier + 1, CurResult)
			end;
		true ->
			prime_factors_next(N, CoMultiplier + 1, CurResult)
	end;

prime_factors_next(_, _, Result) ->
	Result.

is_square_multiple(N) when N < 4 ->
	false;

is_square_multiple(N) ->
	is_square_multiple_next(N, 2).

is_square_multiple_next(N, CoMultiplier) when CoMultiplier =< N ->
	if
		(N rem CoMultiplier) == 0 ->
			IsPrime = is_prime(CoMultiplier),
			if 
				IsPrime == true ->
					if
						(N rem (CoMultiplier * CoMultiplier)) == 0 ->
							true;
						true ->
							is_square_multiple_next(N, CoMultiplier + 1)
					end;
				true ->
					is_square_multiple_next(N, CoMultiplier + 1)
			end;
		true ->
			is_square_multiple_next(N, CoMultiplier + 1)
	end;

is_square_multiple_next(_, _) ->
	false.

find_square_multiples(Count, MaxN) ->
	find_square_multiples_next(0, fail, 0, Count, MaxN).

find_square_multiples_next(CurNum, FirstInLine, FoundNum, NeedFind, SearchLimit) 
when CurNum =< SearchLimit , FoundNum < NeedFind ->
	IsSuitable = is_square_multiple(CurNum),
	if
		IsSuitable == true ->
			if
				FirstInLine == fail ->
					find_square_multiples_next(CurNum + 1, CurNum, FoundNum + 1, NeedFind, SearchLimit);
				true ->
					find_square_multiples_next(CurNum + 1, FirstInLine, FoundNum + 1, NeedFind, SearchLimit)
			end;
		true ->
			find_square_multiples_next(CurNum + 1, fail, 0, NeedFind, SearchLimit)
	end;

find_square_multiples_next(_, FirstInLine, FoundNum, NeedFind, _) when FoundNum == NeedFind ->
	FirstInLine;

find_square_multiples_next(_, _, _, _, _) ->
	fail.

